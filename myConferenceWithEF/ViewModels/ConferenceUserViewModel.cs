﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Models;

namespace myConferenceWithEF.ViewModels
{
    public class ConferenceUserViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        //public string Password { get; set; }
        public string Email { get; set; }
        public string AvatarPath { get; } = $"{Guid.NewGuid().ToString()}.png";
        public string City { get; set; }

        public ConferenceUserViewModel()
        {
            
        }
        public ConferenceUserViewModel(ConferenceUser conferenceUser)
        {
            FirstName = conferenceUser.FirstName;
            LastName = conferenceUser.LastName;
           // Password = conferenceUser.Password;
            Email = conferenceUser.Email;
            AvatarPath = conferenceUser.AvatarPath;
        }

        public ConferenceUser ConvertToUser() => new ConferenceUser()
        {
            ID = Guid.NewGuid(),
            FirstName = FirstName,
            LastName = LastName,
           // Password = Password,
            Email = Email,
            AvatarPath = AvatarPath
        };
    }
}