﻿namespace myConferenceWithEF.ViewModels
{
    public class EditDeleteConferenceUserViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}