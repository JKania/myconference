﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Models;

namespace myConferenceWithEF.ViewModels
{
    public class AddEditConferenceUserViewModel
    {
        public AddEditConferenceUserViewModel(IEnumerable<ConferenceUser> users, IEnumerable<City> cities)
        {
            Cities = cities.Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.ID.ToString()
                }
            ).ToList();
            Users = users;
        }

        public List<SelectListItem> Cities { get; }
        public IEnumerable<ConferenceUser> Users { get; }
        public SelectListItem SelectedCity { get; set; }
    }
}