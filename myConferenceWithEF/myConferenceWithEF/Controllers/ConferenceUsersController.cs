﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using myConferenceWithEF.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Models;
using Services;
using static myConferenceWithEF.ViewModels.ConferenceUserViewModel;

namespace myConferenceWithEF.Controllers
{
    public class ConferenceUsersController : Controller
    {
        //private readonly EfRepository<ConferenceUser> _efRepository;
        private readonly IRepository<ConferenceUser> _conferenceUserRepository;
        private readonly IRepository<City> _citiesRepository;

        public ConferenceUsersController(ConferenceContext context)
        {
            _conferenceUserRepository = new EfRepository<ConferenceUser>(context);
            _citiesRepository = new EfRepository<City>(context);
        }

        // GET: ConferenceUsers
        public async Task<IActionResult> Index()
        {

            return View(_conferenceUserRepository.GetAllToView<ConferenceUserViewModel>());
        }

        // GET: ConferenceUsers/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var conferenceUser = _conferenceUserRepository.GetSingle((Guid)id);
            if (conferenceUser == null)
            {
                return NotFound();
            }

            return View(conferenceUser);
        }

        // GET: ConferenceUsers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ConferenceUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[Bind("FirstName,LastName,Email,AvatarPath")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ConferenceUserViewModel conferenceUserVM, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                var conferenceUser = conferenceUserVM.ConvertToUser();
                _conferenceUserRepository.Add(conferenceUser);

                if (file.Length > 0)
                {
                    string path = $"wwwroot/images/{conferenceUserVM.AvatarPath}";
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
            }
            return RedirectToAction(nameof(Index));
        }

        public ActionResult GetSingle()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSingleToDelete(EditDeleteConferenceUserViewModel conferenceUser)
        {
            var selectedUser = _conferenceUserRepository
                .GetAll()
                .FirstOrDefault(f =>
                 f.FirstName == conferenceUser.FirstName && f.LastName == conferenceUser.LastName);
            var ID = selectedUser.ID;
            return RedirectToAction("Delete", new { ID });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetSingleToUpdate(EditDeleteConferenceUserViewModel conferenceUser)
        {
            var selectedUser = _conferenceUserRepository
                .GetAll()
                .FirstOrDefault(f =>
                    f.FirstName == conferenceUser.FirstName && f.LastName == conferenceUser.LastName);
            var ID = selectedUser.ID;
            return RedirectToAction("Edit", new { ID });
        }
        // GET: ConferenceUsers/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var conferenceUser = _conferenceUserRepository.GetSingle((Guid)id);
            if (conferenceUser == null)
            {
                return NotFound();
            }
            return View(conferenceUser);
        }

        // POST: ConferenceUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[Bind("FirstName,LastName,Email,AvatarPath")] 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ConferenceUser conferenceUser)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _conferenceUserRepository.Edit(conferenceUser);
                }
                catch (DbUpdateConcurrencyException)
                {
                    return NotFound();
                }
                return RedirectToAction(nameof(Index));
            }
            return View(conferenceUser);
        }

        // GET: ConferenceUsers/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var conferenceUser = _conferenceUserRepository.GetSingle((Guid)id);
            if (conferenceUser == null)
            {
                return NotFound();
            }

            return View(conferenceUser);
        }

        // POST: ConferenceUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var conferenceUser = _conferenceUserRepository.GetSingle(id);
            _conferenceUserRepository.Delete(conferenceUser);
            return RedirectToAction(nameof(Index));
        }
    }
}
