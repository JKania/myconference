﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models;
using Services;

namespace myConferenceWithEF.Controllers
{
    public class ConferenceController : Controller
    {
        private readonly ConferenceContext _context;

        public ConferenceController(ConferenceContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            
            ViewBag.Message = "Chujdupakurwacipa";
            return View();
        }
       
        [HttpGet]
        public IActionResult SaveUser()
        {
            //ConferenceList.GuestList
           
            return View(_context.ConferenceUsers.ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SaveUser(ConferenceUser user, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                string path = string.Empty;
                if (file.Length > 0)
                {
                    path = $"wwwroot/images/{file.FileName}";
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
                user.AvatarPath = $"{file.FileName}";

                _context.ConferenceUsers.Add(user);
                _context.SaveChanges();
                TempData["ResultMessage"] = "Dodanie uzytkownika zakonczone sukcesem!";
                return RedirectToAction(nameof(SaveUser));
            }
            return View(user);
        }
    }
}