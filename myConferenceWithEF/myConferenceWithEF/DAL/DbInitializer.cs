﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Services;

namespace myConferenceWithEF.DAL
{
    public static class DbInitializer
    {
        public static void Initialize(ConferenceContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }
    }
}
