﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Models;

namespace Services
{
    public class ConferenceContext :DbContext
    {
        public ConferenceContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<ConferenceUser> ConferenceUsers { get; set; }
        public DbSet<City> Miejscowoscis { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ConferenceUser>().ToTable("ConferenceUser");
            modelBuilder.Entity<City>().ToTable("City");
        }
    }
}
