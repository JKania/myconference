﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using myConferenceWithEF.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Models;

namespace Services
{
    public class EfRepository<T> : IRepository<T> where T : class
    {
        protected DbSet<T> DbSet;
        protected DbContext Context;

        public EfRepository(DbContext context)
        {
            DbSet = context.Set<T>();
            Context = context;
        }
        public void Add(T entity)
        {
            DbSet.Add(entity);
            Context.SaveChanges();
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
            Context.SaveChanges();
        }

        public void Edit(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public T GetSingle(Guid ID)
        {
            return DbSet.Find(ID);
        }

        public IQueryable<T> GetAll()
        {
            return DbSet;
        }
        public ConferenceUserViewModel GetSingleToView(Guid ID)
        {
            var set = DbSet as DbSet<ConferenceUser>;
            var user = set.FirstOrDefault(i => i.ID == ID);
            var userView = new ConferenceUserViewModel(user);

            return userView;
        }
        public IEnumerable<G> GetAllToView<G>()
        {
            return DbSet.Select(x => (G) Activator.CreateInstance(typeof(G), x));
            var list = new List<G>();
            foreach (var set in DbSet)
            {
                var user = set;

                list.Add((G)Activator.CreateInstance(typeof(G), user));
            }
            return list;
        }
    }
}
