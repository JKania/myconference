﻿using System;
using System.Collections.Generic;
using System.Linq;
using myConferenceWithEF.ViewModels;

namespace Services
{

    public interface IRepository<T> where T : class
    {
        void Add(T entity);
        void Delete(T entity);
        void Edit(T entity);
        T GetSingle(Guid ID);
        IQueryable<T> GetAll();
        IEnumerable<G> GetAllToView<G>();
        ConferenceUserViewModel GetSingleToView(Guid ID);
    }
}