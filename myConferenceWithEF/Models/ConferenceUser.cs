﻿using System;

namespace Models
{
    public class ConferenceUser
    {
        public enum ConferenceType
        {
            Conference,
            Workshop
        }
        public Guid ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string AvatarPath { get; set; }

        public City City { get; set; }
        public ConferenceType? Type { get; set; }

        public ConferenceUser()
        {
            ID = Guid.NewGuid();
        }
    }
}
