﻿using System;
using System.Collections.Generic;

namespace Models
{
    public class City
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public ICollection<ConferenceUser> Users { get; set; }
        public City()
        {
            ID = Guid.NewGuid();
        }
    }
}